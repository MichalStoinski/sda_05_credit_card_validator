package sda.cards.issuersRules;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class IssueDetectorImplTest {

    // create new IIssuerDetector object
    private IIssuerDetector detector = new IssueDetectorImpl();
    private String cardNo;
    private String issuer;

    public IssueDetectorImplTest(String cardNo, String issuer) {
        this.cardNo = cardNo;
        this.issuer = issuer;
    }

    @Parameters
    public static Collection<Object[]> setData() {
        return Arrays.asList(new Object[][]{
                {"4977807180348236", "Visa"},
                {"5577807180348236", "Master Card"},
                {"1234567812345678", "SDA_test"},
                {"377484858231211", "American Express"}
        });
    }

    @Test
    public void detectIssuerTest() throws IOException {
        String result = detector.detectIssuer(this.cardNo);
        assertEquals(this.issuer, result);
    }
}