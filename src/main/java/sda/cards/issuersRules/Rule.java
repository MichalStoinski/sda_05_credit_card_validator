package sda.cards.issuersRules;

public class Rule {

    private final int prefix; // umber of decimal digits is unknown
    private final int length; // length of card number
    private final String issuer;

    public Rule(int prefix, int length, String issuer) {
        this.prefix = prefix;
        this.length = length;
        this.issuer = issuer;
    }

    public int getPrefix() {
        return prefix;
    }

    public int getLength() {
        return length;
    }

    public String getIssuer() {
        return issuer;
    }


    /**
     * Override for testing purposes
     *
     * @param r the other Rule object to compare
     * @return true
     */
    @Override
    public boolean equals(Object r) {
        return r instanceof Rule
                && this.getLength() == ((Rule) r).getLength()
                && this.getPrefix() == ((Rule) r).getPrefix()
                && this.getIssuer().equals(((Rule) r).getIssuer());
    }
}
