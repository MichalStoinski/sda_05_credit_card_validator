package sda.cards;

import java.io.IOException;


/**
 * How to call this program from console (copy this two lines)
 * C:\Users\User\IdeaProjects\sda-intermediate\target\classes>java sda.cards.App 5302366647634384
 * C:\Users\User\IdeaProjects\sda-intermediate\src\main\resources\CardIssuersList.txt
 * c:\Users\Michał\IdeaProjects\SDA_05_credit_card_validator\src\main\resources\CardIssuersList.txt
 *
 * Card Validator System
 * ISSUER: Master Card
 * VALID: TAK
 */
public class App {
    public static void main(String[] args) throws IOException {
        // get card number from first argument of calling App
        String cardNo = args[0];

//        String ruleBaseFilePath = "C:\\Users\\User\\IdeaProjects\\sda-intermediate\\src\\main\\resources\\CardIssuersList.txt";
        String ruleBaseFilePath = args[1];

        System.out.println("Card Validator System");
        ICardValidator validator = new SDACardValidator(ruleBaseFilePath);
        ValidationResult result = validator.validateCardNo(cardNo);

        // get and print results
        System.out.println("ISSUER: " + result.getIssuer());
        System.out.println("VALID : " + (result.getIsValid() ? "TAK" : "NIE"));
    }
}
