package sda.cards.fileReader;

import sda.cards.issuersRules.Rule;

import java.util.ArrayList;
import java.util.List;

public class SeedRulesBuilder implements IRuleReader {

    /**
     * Default list of issuers rules
     * @return list of sample rules (hard-coded)
     */
    @Override
    public List<Rule> buildRules() {
        List<Rule> rules = new ArrayList<>();
        // first rule only for testing purposes
        rules.add(new Rule(12345678, 16, "SDA_test"));
        rules.add(new Rule(4,16,"Visa"));
        rules.add(new Rule(51,16,"Master Card"));
        rules.add(new Rule(52,16,"Master Card"));
        rules.add(new Rule(53,16,"Master Card"));
        rules.add(new Rule(54,16,"Master Card"));
        rules.add(new Rule(55,16,"Master Card"));
        rules.add(new Rule(34,15,"American Express"));
        rules.add(new Rule(37,15,"American Express"));
        return rules;
    }
}
