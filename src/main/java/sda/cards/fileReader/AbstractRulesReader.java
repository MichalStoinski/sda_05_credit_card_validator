package sda.cards.fileReader;

import sda.cards.issuersRules.Rule;

import java.io.IOException;
import java.util.List;

public abstract class AbstractRulesReader implements IRuleReader {

    final String pathToFile;

    /**
     * Constructor used to set path to txt file
     * @param pathToFile path to file with card issuers rules
     */
    AbstractRulesReader(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    @Override
    public abstract List<Rule> buildRules() throws IOException;
}
