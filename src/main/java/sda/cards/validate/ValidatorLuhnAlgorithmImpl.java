package sda.cards.validate;

public class ValidatorLuhnAlgorithmImpl implements IValidatorAlgorithm {

    public ValidatorLuhnAlgorithmImpl() {
    }

    public boolean validate(String cardNo) {

        int size = cardNo.length();
        int[] numbers = new int[size];

        // parsing chars into integers
        for (int i = 0; i < size; i++) {
//            numbers[i] = cardNo.charAt(i) - 48;
            numbers[i] = Character.getNumericValue(cardNo.charAt(i));
        }

        int sum = 0;
        boolean alt = false;
        for (int i = 0; i < size; i++) {
            int digit = numbers[size - 1 - i];
            if (alt) {
                digit *= 2;
                if (digit > 9) {
                    // add digits
                    digit -= 9;
                }
            }
            alt = !alt;
            sum += digit;
        }
        return sum % 10 == 0;
    }
}
