package sda.cards.validate;

public interface IValidatorAlgorithm {

    /**
     * Validate card number with specify algorithm
     * @param cardNo card number
     * @return result of validation
     */
    public boolean validate(String cardNo);
}
